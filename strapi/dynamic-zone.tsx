import dynamicInventory from "@/components/dynamic-inventory";

export const renderDynamicZone = (dynamicZoneInfo, pageAttributes, dynamicPagePath /*contains info about current location */, baseUrl) => {
    if(Array.isArray(dynamicZoneInfo) && dynamicZoneInfo.length > 0) {
        return dynamicZoneInfo.map((componentInfo, index) => {

            const {__component, ...dynamicComponentProps} = componentInfo

            if(__component in dynamicInventory) {
                const DynamicZoneComponent = dynamicInventory[__component].component
                const requiresPageAttributes = typeof dynamicInventory[__component].requiresPageAttributes !== 'undefined' && dynamicInventory[__component].requiresPageAttributes

                return <DynamicZoneComponent
                    key={`${__component}.${index}`}
                    pageAttributes={
                        requiresPageAttributes ? pageAttributes : undefined
                    }
                    pagePath={
                        requiresPageAttributes ? dynamicPagePath : undefined
                    }
                    baseUrl={
                        requiresPageAttributes ? baseUrl : undefined
                    }
                    {...dynamicComponentProps}
                />

            }

            return null
        })
    }

    return null
}
