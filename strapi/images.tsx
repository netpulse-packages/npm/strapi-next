export type imageResult = {
    alt?: string;
    url: string;
}
export function getFirstImage(mediaProp):imageResult | undefined {
    if(mediaProp !== null && typeof mediaProp !== 'undefined' && mediaProp.data !== null && typeof mediaProp.data !== 'undefined') {

        const {data} = mediaProp
        const firstImage = Array.isArray(data) ? data[0] : data
        const {attributes} = firstImage
        const {url, alternativeText} = attributes

        return {url, alt: alternativeText}
    }

    return undefined
}
