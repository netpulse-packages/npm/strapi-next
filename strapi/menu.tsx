export const calculateUrls = (menuPages, mainMenuItemsOnly: boolean = false, urlPrefix:string = null) => {
    return menuPages.reduce(function(result, menuPage, index) {

        const {attributes} = menuPage

        const {slug, title, children, visibleInMainMenu} = attributes

        const url = urlPrefix ==null ? `/${slug}` : `${urlPrefix}/${slug}`
        const submenu = children.data.length > 0 ? calculateUrls(children.data, mainMenuItemsOnly, url) : null

        if(!mainMenuItemsOnly || visibleInMainMenu)
            result.push({title, url, submenu, visibleInMainMenu})

        return result;
    }, [])
}
const setLocaleUrl = (localizedUrls, locale, slug) => {

    if(locale in localizedUrls) {
        localizedUrls[locale] = `${localizedUrls[locale]}/${slug}`
    } else {
        localizedUrls[locale] = `/${slug}`
    }

    return localizedUrls
}

export const creatIdUrlMap = (nestedPages, idMap = {}, parentUrls:object = {}) => {
    nestedPages.forEach(menuPage => {
        const {attributes, id} = menuPage
        const {slug, children, localizations, locale} = attributes

        //only for direct children
        const parentMap = {
          /* 'en' : '/some/parent/url',
             'nl' : '/een/bovenliggende/url' */
        }

        const url = (locale in parentUrls === false) ? `/${slug}` : `${parentUrls[locale]}/${slug}`
        parentMap[locale] = url

        if(typeof localizations !== 'undefined' && localizations.data.length > 0 ) {
            localizations.data.forEach(localization => {
                const {attributes, id} = localization
                const {slug, locale} = attributes
                const url = (locale in parentUrls === false) ? `/${slug}` : `${parentUrls[locale]}/${slug}`

                idMap[id] = url
                parentMap[locale] = url
            });
        }

        if(typeof children !== 'undefined' && children.data.length > 0) {
            //notice setting id map is not really necessary as objects are pointers, but for readability aye.
            idMap = creatIdUrlMap(children.data, idMap,  parentMap)
        }

        idMap[id] = url

    });

    return idMap
}

export const findUrlBySlug = (menuPages, targetSlug:string, urlPrefix:string = null) => {

    for (let i = 0; i < menuPages.length; i++) {
        const {attributes} = menuPages[i]
        const {slug, children, localizations, locale} = attributes


        const url = urlPrefix ==null ? `/${slug}` : `${urlPrefix}/${slug}`
        if(slug == targetSlug)
            return url

        if(typeof localizations !== 'undefined' && localizations.data.length > 0 ) {
            const localizedUrl = findUrlBySlug(localizations.data, targetSlug, url)
            //console.log('localizedUrl :', localizedUrl, targetSlug, url)
        }

        const childUrl = (typeof children !== 'undefined' && children.data.length > 0) ? findUrlBySlug(children.data, targetSlug, url) : null
        if(childUrl)
            return childUrl
    }

    return null
}
