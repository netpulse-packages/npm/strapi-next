import getConfig from 'next/config'
import qs from 'qs'

// Only holds serverRuntimeConfig and publicRuntimeConfig, AND MORE
const { publicRuntimeConfig } = getConfig()
const pageContentPopulationFields = [
    'header',
    'header.background',
    'header.text',
    'content.textTiles.background',
    'content.iconTexts.icon',
    'content.contactSection', /* note: is in content.quotation-form.contactSection */
    'content.products', /* note: is in content.quotation-form.products */
    'content.steps', /* note: is in content.quotation-form.products */
    'content.stepLabels', /* note: is in content.quotation-form.products */
    'children.header.background',
    'localizations'
]

export function apiURI() {
    return `${publicRuntimeConfig.strapi.baseURI}/api`
}


function getSingleResultLocalized(
    strapiResponse,
    locale:string
){
    const {meta, data} = strapiResponse

    let dataResponse = data[0];//use language of page with slug 'home' as fallback, (usually english but could defer)
    if(dataResponse?.attributes?.locale != locale) {
        //console.log('find different language content')
        const localizedData = data[0]?.attributes?.localizations?.data.filter(e => e.attributes.locale == locale)
        if(localizedData && Array.isArray(localizedData) && localizedData.length>0) {
            dataResponse = localizedData[0]
            //console.log('found different language content', dataResponse)
        }
    }

    const {pagination} = meta
    const {total} = pagination

    if(total==0)
        return null

    return dataResponse
}

function getSingleResult(
    strapiResponse
){
    const {meta, data} = strapiResponse
    const {pagination} = meta
    const {total} = pagination

    if(total==0)
        return null

    return data[0] //always return the first match
}


export async function getNestedPagesByLocale(locale: string = 'nl') {

    /* other localizations are only loaded so we can redirect the user to another language of the same page */
    const query = qs.stringify({
        locale,
        populate: [ /* atm 4 levels deep max */
            'localizations',
            'children.localizations',
            'children.children.localizations',
            'children.children.children.localizations',
            'children.children.children.children.localizations',
            /* todo only select required fields */
        ],
        sort: [
            'orderId:asc'
        ],
        filters: {
            parents: {
                id: {
                    $null: true,
                }
            },
        },
    }, {
        encodeValuesOnly: true,
    });


    const rawRes = await fetch(`${apiURI()}/pages?${query}`)
    const res  = await rawRes.json()
    return res?.data
}

export async function getFooter(locale: string = 'nl') {

    const query = qs.stringify({
        locale,
        populate: [
            'bottomSection.parents.parents.parents.parents',
            'serviceSection.parents.parents.parents.parents',
        ]
    }, {
        encodeValuesOnly: true,
    });

    const res = await fetch(`${apiURI()}/footer?${query}`)
    return await res.json()
}

export async function getCompanyDetails(locale: string = 'nl') {
    const query = qs.stringify({
        locale
    }, {
        encodeValuesOnly: true,
    });

    const res = await fetch(`${apiURI()}/company-detail?${query}`)
    return await res.json()
}

//http://strapi.netpulse.netpulse-dev.nl/api/company-detail

export async function getHomePage(locale: string = 'nl'){
    let localizedPageContentPopulationFiels = null

    //load all localizations fully for homepage..
    const localizedContentPopulationFields = pageContentPopulationFields.map(locPopulateField => `localizations.${locPopulateField}`)
    localizedContentPopulationFields.push('localizations.localizations')

    localizedPageContentPopulationFiels = [
        ...pageContentPopulationFields.filter(e => e !== 'localizations'),
        ...localizedContentPopulationFields
    ]

    const queryObj = {
        locale: 'en', //if slug empty get the english homepage (and all lang variations with it)
        populate: localizedPageContentPopulationFiels,
        filters: {
            slug: {
                $eq: 'home',
            },
        },
    };

    const query = qs.stringify(queryObj, {
        encodeValuesOnly: true,
    });

    const res = await fetch(`${apiURI()}/pages?${query}`)
    const strapiResponse = await res.json()

    return getSingleResultLocalized(strapiResponse, locale)
}

export async function getPageInfo(slugs = [], locale: string = 'nl') {

    if(slugs.length == 0)
        return getHomePage(locale)

    const queryObj = {
        locale: 'all', //if slug empty get the english homepage (and all lang variations with it)
        populate: pageContentPopulationFields,//get all lang variations if page = "/" aka home
        filters: {
            slug: {
                $eq: slugs,
            },
        },
    };

    let slugPointer = queryObj.filters
    //add the slugs filter, the slug must match the page as well as the parent (this allows for some duplicates if parents differ
    for (let i = slugs.length - 1; i >= 0; i--) {
        slugPointer.slug = {
            $eq: slugs[i],
        }

        //prepare the parents filter for the next iteration (if any)
        //set the pointer to the new parent filter
        if(i>0) {
            slugPointer['parents'] = {}
            slugPointer = slugPointer['parents']
        }
    }

    //console.log('queryObj', JSON.stringify(queryObj, undefined, 4));

    const query = qs.stringify(queryObj, {
        encodeValuesOnly: true,
    });

    const res = await fetch(`${apiURI()}/pages?${query}`)
    const strapiResponse = await res.json()

   return getSingleResult(strapiResponse)

}

export async function getLocales() {
    const res = await fetch(`${apiURI()}/i18n/locales`)
    return await res.json()
}

/*export async function getPortfolioItems(portfolioPageSlug: string = 'portfolio') {
    return getChildPages(portfolioPageSlug)
}

export async function getChildPages(parentPageSlug: string) {

    const query = qs.stringify({
        populate: pageContent,
        filters: {
            parents: {
                slug: {
                    $eq: parentPageSlug,
                },
            }
        },
    }, {
        encodeValuesOnly: true,
    });

    const res = await fetch(`${apiURI()}/pages?${query}`)
    return await res.json()
}*/
