export const findSlugForLocalization = (localizations, targetLocale:string) => {
    for (let i = 0; i < localizations.length; i++) {
        const {attributes} = localizations[i]
        const {slug, locale} = attributes


        if(locale == targetLocale)
            return slug
    }

    return null
}

export const findIDForLocalization = (localizations, targetLocale:string) => {
    for (let i = 0; i < localizations.length; i++) {
        const {attributes, id} = localizations[i]
        const {locale} = attributes

        if(locale == targetLocale)
            return id
    }

    return null
}
