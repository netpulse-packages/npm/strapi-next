export const urlIsAbsolute = (url) => {
    return url.indexOf('://') > 0 || url.indexOf('//') === 0
}

//since sorting with strapi api on children was kinda hard..
export const sortByOrderId = ( a, b ) => {
    if(a.attributes.orderId == null) {
        return 1;
    }
    if(b.attributes.orderId == null) {
        return -1;
    }

    if ( a.attributes.orderId < b.attributes.orderId ){
        return -1;
    }
    if ( a.attributes.orderId > b.attributes.orderId ){
        return 1;
    }

    return 0;
}
