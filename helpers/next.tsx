//@ts-nocheck
// todo delete line above at some point
import { useRouter as nextUseRouter } from 'next/router'
import { useState, useEffect } from 'react'
import qs from 'qs'

import {NextRouter} from "next/dist/shared/lib/router/router";
import {
    paramCase
} from "change-case";
import {ParsedUrlQueryInput} from "node:querystring";


type NextExtRouter = NextRouter & {
    cleanQuery?: any,
    path?: string,
    getQueryParam?: (key: string, paramCaseKeys?:boolean) =>  string | null | ParsedUrlQueryInput | undefined;
    setQueryParams?: (params: string | null | ParsedUrlQueryInput | undefined, paramCaseKeys?:boolean, method?: 'push' | 'replace') =>  Promise<boolean>;
    getUrl: (extraParams?: object) =>  string;
}

type ParamObject = {
    [key: string]: string
}


export const useQueryState = (params: ParamObject) => {
    //todo params should be treated as initStateParams()
    const router = useRouter();

    const {setQueryParams} = router;
    const [stateParams, setStateParams] = useState(params);

    const setParams = (paramsNew, updateQueryParams = true) => {
        setStateParams(stateParams => {return {...stateParams, ...paramsNew}})
        if(updateQueryParams)
            setQueryParams(paramsNew)
    }

    //only adds this param to the query instead of all values defined in stateParams
    const setStateParam = (key: string, value: string) => {
        const newState = {...stateParams, [key]:value}
        setStateParams(newState)

        const queryParams = {...router.cleanQuery, [key]:value}
        setQueryParams(queryParams)
    }

    const initStateParams = (paramsNew) => {

        /*
        * Try to set from query params first, fallback on default params
        * */
        const finalParams = {}
        const keys = Object.keys(paramsNew);
        keys.forEach((key, index) => {
            finalParams[key] = key in router.cleanQuery ? router.cleanQuery[key] : paramsNew[key]
        });


        setParams(finalParams, false)
    }

    const getStateParam = (key: string, defaultValue: string) => {
        return stateParams[key] || defaultValue
    }

    /*
    * Sync state from previous / next browser buttons etc.
    * */
    useEffect(() => {
        const handleRouteChange = (url, { shallow }) => {
            const queryStr =  url.split('?')[1];
            const newState = qs.parse(queryStr)
            //console.log('routeChangeComplete', newState)
            setParams(newState, false)
        }

        router.events.on('routeChangeComplete', handleRouteChange)

        // If the component is unmounted, unsubscribe
        // from the event with the `off` method:
        return () => {
            router.events.off('routeChangeComplete', handleRouteChange)
        }
    }, [router.events])

    return {stateParams, setStateParams: setParams, getStateParam, initStateParams, setStateParam}
}

export function useRouter(): NextExtRouter {
    const nextRouter = nextUseRouter()
    const router = {
        cleanQuery: [],
        path: undefined, // set below
        ...nextRouter,
        getQueryParam: (key, paramCaseKeys = true) => {
            return router.query[paramCaseKeys ? paramCase(key) : key]
        },
        getUrl: (extraParams: object = null) => {
           const params = extraParams==null?{...router.cleanQuery}:{...router.cleanQuery, ...extraParams}
           return `${router.path}?${qs.stringify(params)}`
        },
        setQueryParams: (params, paramCaseKeys = true, method = 'replace') => {

            const finalParams = paramCaseKeys ? {} : params
            if(paramCaseKeys) {
                for (const key in params)
                {
                    finalParams[paramCase(key)] = params[key]
                }
            }

            return router[method]({
                query: { ...router.query, ...finalParams },
            },undefined, { scroll: false });
        }
    }
    const {dynamicPagePath, ...query} = router.query
    // @ts-ignore
    router.cleanQuery = query

    const pathArray = Array.isArray(router.query.dynamicPagePath) ? router.query.dynamicPagePath : [router.query.dynamicPagePath]
    router['path'] = `/${pathArray.join('/')}`

/*    router.push({
        query: { ...router.query, 'product-title': productTitle.toLowerCase() },
    },undefined, { scroll: false });*/
    // @ts-ignore
    return router
}