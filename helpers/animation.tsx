import React, {useState, useEffect} from 'react';
import TypeIn from '@/components/text/effects/type-in'
import BaffleIn from '@/components/text/effects/baffle-in';
// This is a remark plugin
/*function markDownAnimate(options = {textEffect: null}) {

    const {textEffect} = options

    return function transformer(tree, file) {
        if(textEffect) {
            tree.children.map(child => {
                console.log(child.type, child.value)
                child.children.map(child => {
                    switch(textEffect) {
                        case "typeIn":{
                            child.value = child.value + "nibba"
                            child.children = [<Logo />]
                            console.log(child.type, child.value, textEffect)
                            break;
                        }
                    }

                })
            })
        }

    };
    export default markDownAnimate;
}*/

const effectToComponentMap = {
    'typeIn' : TypeIn,
    'baffleIn' : BaffleIn
}

export const elementMapper = (textEffect) => {
    const animatedComponentsWrapper = ({node, children,  ...props}) => {
        const Element = node.tagName;
        const TextEffectComponent = effectToComponentMap[textEffect]

        return children

        // return  <Element style={{color: 'red'}} {...props}>{children.map((child, index) => {
        
        //     return (typeof child === 'string') ? <TextEffectComponent text={child} /> : child

        // })}</Element>
    }

    return {
        a: animatedComponentsWrapper,
        blockquote: animatedComponentsWrapper,
        //br: animatedComponentsWrapper,
        code: animatedComponentsWrapper,
        em: animatedComponentsWrapper,
        h1: animatedComponentsWrapper,
        h2: animatedComponentsWrapper,
        h3: animatedComponentsWrapper,
        h4: animatedComponentsWrapper,
        h5: animatedComponentsWrapper,
        h6: animatedComponentsWrapper,
        hr: animatedComponentsWrapper,
        //img: animatedComponentsWrapper,
        //ul: animatedComponentsWrapper,
        li: animatedComponentsWrapper,
        ol: animatedComponentsWrapper,
        p: animatedComponentsWrapper,
        pre: animatedComponentsWrapper,
        strong: animatedComponentsWrapper
    }
}

