import qs from 'qs'

export const getLinkedInShareLink = (absoluteUrl, title, description, source = null) => {
    const params = {
        url: absoluteUrl,
        mini: true,
        title,
        summary: description
    };

    if(source)
        params[source] = source

    const query = qs.stringify(params, {
        encodeValuesOnly: true,
    });

    return `https://www.linkedin.com/shareArticle?${query}`
}

export const getTwitterShareLink = (absoluteUrl, title, description)=> {

    const query = qs.stringify({
        text: `
            ${title}
            
            ${description},
            
            ${absoluteUrl}
        `,
    }, {
        encodeValuesOnly: true,
    });
    return ` https://twitter.com/intent/tweet?${query}`
}
