import RichText from "@/components/text/rich-text";
import * as React from "react";

export const linkItems = (children:Array<string> = null) => {
    if(!Array.isArray(children) || children.length === 0)
        return null

    const regExButtons = /(btn:)/g;//match buttons

    let text = children[0]// make this for

    const button = text.match(regExButtons);
    if(button) {
        children[0] = text.replace(regExButtons, '')
    }

    return {tagName: button ? 'button': 'a', children}
}

export const pricedListItem = (children:Array<string> = null) => { //priceFormatter:(string) => string
    if(!Array.isArray(children) || children.length === 0)
        return null

    //match spaces around
    const regExRequired = /^[ ]*?[\*]/g;//match asterisk at the beginning
    const regExpPrice = /[ ]+[0-9.]+$/g;//number at the end of the string
    const regExpQuantity = /^[0-9.]+[ ]/g;//number at beginning

    let text = children[0]

    const required = text.match(regExRequired);
    if(required)
        text = text.replace(regExRequired, '')

    const price = text.match(regExpPrice);
    const quantity = text.match(regExpQuantity)

    if(price)
        text = text.replace(regExpPrice, '')

    if(quantity)
        text = text.replace(regExpQuantity, '');

    return {text, price: price ? parseFloat(price.toString()) : null, quantity: quantity ? parseInt(quantity.toString()) : null, required: required !== null}
}