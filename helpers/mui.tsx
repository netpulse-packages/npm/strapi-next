import {ResponsiveStyleValue} from "@mui/system";
import {GridSpacing} from "@mui/material/Grid/Grid";
import * as React from "react";
import {GridSize} from "@mui/material/Grid";

export type GridSizes = {
    tileXs?: boolean | GridSize;
    tileSm?: boolean | GridSize;
    tileMd?: boolean | GridSize;
    tileLg?: boolean | GridSize;
    tileXl?: boolean | GridSize;
}
export const containerGridToGrid = ({
                                        tileXs,
                                        tileSm,
                                        tileMd,
                                        tileLg,
                                        tileXl
                                    } : GridSizes
) => {
    return { // these grid breakpoints serve as a default for all TextTileChildren
        ...(tileXs && {xs: tileXs}),
        ...(tileSm && {sm: tileSm}),
        ...(tileMd && {md: tileMd}),
        ...(tileLg && {lg: tileLg}),
        ...(tileXl && {xl: tileXl}),
    }
}