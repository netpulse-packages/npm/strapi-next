import cookie from 'cookie'

export const parseCookies = (req = null) => {
    return cookie.parse(req ? req.headers.cookie || "" : document.cookie);
}
